import React from 'react';

export default function Button({ children, color = "blue", ...props }) {
    return <button className={`bg-${color}-500 hover:bg-${color}-700 text-white font-bold py-2 px-4 rounded`} {...props}>
        {children}
    </button>
}