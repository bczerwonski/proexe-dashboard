import React from 'react';
import { Field as FormikField, ErrorMessage } from 'formik';

export default function Field({ name, type = "text" }) {
    return <div className="flex flex-col">
        <label className="capitalize font-medium" htmlFor={name} >{name}</label>
        <FormikField className="focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-7 pr-12 sm:text-sm border-gray-300 border-2 rounded-md h-8" type={type} name={name} />
        <ErrorMessage className="text-red-600 font-normal" name={name} component="div" />
    </div>
}